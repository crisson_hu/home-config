alias ls='ls -F'
alias ll='ls -Fls'
alias la='ls -FAl'
alias l='ls -Fal'

alias mv='mv -i'
alias cp='cp -i'

alias grep='grep --color'
alias less='less -ri'

# Configuration for specific apps.
alias screen='screen -e ^gg'
alias wget='wget -t0 -np -c'

[ -f ~/.bash_aliases_misc ] && . ~/.bash_aliases_misc
[ -f ~/.bash_aliases_local ] && . ~/.bash_aliases_local
