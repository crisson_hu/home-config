" Vim color file
" Based on peachpuff.vim
" Maintainer: David Ne\v{c}as (Yeti) <yeti@physics.muni.cz>
" Last Change: 2003-04-23
" URL: http://trific.ath.cx/Ftp/vim/colors/peachpuff.vim

" This color scheme uses a peachpuff background (what you've expected when it's
" called peachpuff?).
"
" Note: Only GUI colors differ from default, on terminal it's just `light'.

" First remove all existing highlighting.
set background=light
hi clear
if exists("syntax_on")
  syntax reset
endif

let colors_name = "lightterm"

hi Normal guibg=White guifg=Black

hi SpecialKey term=bold ctermfg=4 guifg=Blue
hi NonText term=bold cterm=bold ctermfg=4 gui=bold guifg=Blue
hi Directory term=bold ctermfg=4 guifg=Blue
hi ErrorMsg term=standout cterm=bold ctermfg=7 ctermbg=1 gui=bold guifg=White guibg=DeepPink
hi IncSearch term=reverse cterm=reverse gui=reverse
hi Search term=reverse ctermbg=3 guibg=Gold2
hi MoreMsg term=bold ctermfg=2 gui=bold guifg=SeaGreen
hi ModeMsg term=bold cterm=bold gui=bold
hi LineNr term=underline ctermfg=3 guifg=DeepPink3
hi Question term=standout ctermfg=2 gui=bold guifg=SeaGreen
hi StatusLine term=bold,reverse cterm=bold,reverse gui=bold guifg=White guibg=Black
hi StatusLineNC term=reverse cterm=reverse gui=bold guifg=PeachPuff guibg=Gray45
hi VertSplit term=reverse cterm=reverse gui=bold guifg=White guibg=Gray45
hi Title term=bold ctermfg=5 gui=bold guifg=DeepPink3
hi Visual term=reverse cterm=reverse gui=reverse guifg=Grey80 guibg=fg
hi VisualNOS term=bold,underline cterm=bold,underline gui=bold,underline
hi WarningMsg term=standout ctermfg=1 gui=bold guifg=DeepPink
hi WildMenu term=standout ctermfg=0 ctermbg=3 guifg=Black guibg=Yellow
hi Folded term=standout ctermfg=4 ctermbg=7 guifg=Black guibg=#e3c1a5
hi FoldColumn term=standout ctermfg=4 ctermbg=7 guifg=DarkBlue guibg=Gray80
hi DiffAdd term=bold ctermfg=7 ctermbg=4 guibg=White
hi DiffChange term=bold ctermfg=7 ctermbg=5 guibg=#edb5cd
hi DiffDelete term=bold cterm=bold ctermfg=7 ctermbg=6 gui=bold guifg=LightBlue guibg=#f6e8d0
hi DiffText term=reverse cterm=bold ctermfg=7 ctermbg=1 gui=bold guibg=#ff8060
hi Cursor guifg=bg guibg=fg
hi lCursor guifg=bg guibg=fg

" Colors for syntax highlighting
hi Comment term=bold ctermfg=4 guifg=#406090
hi Constant term=bold ctermfg=4 guifg=DarkBlue
hi Special term=bold ctermfg=5 guifg=SlateBlue
hi Identifier term=underline ctermfg=6 guifg=DarkCyan
hi Statement term=bold ctermfg=3 gui=bold guifg=Brown
hi PreProc term=underline ctermfg=5 guifg=Magenta3
hi Type term=underline ctermfg=2 gui=bold guifg=SeaGreen
hi Ignore cterm=bold ctermfg=7 guifg=bg
hi Error term=reverse cterm=bold ctermfg=7 ctermbg=1 gui=bold guifg=White guibg=DeepPink
hi Todo term=standout ctermfg=0 ctermbg=3 guifg=Blue guibg=Yellow

hi SignColumn     term=standout ctermfg=4 ctermbg=7 guifg=DarkBlue guibg=Grey
hi Conceal        ctermfg=7 ctermbg=0 guifg=LightGrey guibg=DarkGrey
hi SpellBad       term=reverse ctermbg=1 gui=undercurl guisp=DeepPink
hi SpellCap       term=reverse ctermbg=4 gui=undercurl guisp=Blue
hi SpellRare      term=reverse ctermbg=5 gui=undercurl guisp=Magenta
hi SpellLocal     term=underline ctermbg=6 gui=undercurl guisp=DarkCyan
hi Pmenu          ctermfg=0 ctermbg=5 guibg=LightMagenta
hi PmenuSel       ctermfg=0 ctermbg=7 guibg=Grey
hi PmenuSbar      ctermbg=7 guibg=Grey
hi PmenuThumb     ctermbg=0 guibg=Black
hi TabLine        term=underline cterm=bold,underline ctermfg=4 ctermbg=3 gui=underline guibg=LightGrey
hi TabLineSel     term=bold cterm=bold ctermfg=0 ctermbg=3 gui=bold
hi TabLineFill    term=reverse cterm=bold,reverse ctermfg=2 ctermbg=2 gui=reverse
hi CursorColumn   term=reverse ctermbg=7 guibg=Grey90
"hi CursorLine     term=underline cterm=bold ctermfg=7 ctermbg=3 guifg=white guibg=darkyellow
hi CursorLine     term=underline cterm=underline
hi ColorColumn    term=reverse ctermbg=2 guibg=LightRed
hi MatchParen     term=reverse ctermbg=6 guibg=Cyan
hi Underlined     term=underline cterm=underline ctermfg=5 gui=underline guifg=SlateBlue
hi String         term=standout cterm=bold ctermfg=1 gui=bold guifg=DeepPink
