"if filereadable("tags") || filereadable("GTAGS")
  "set mouse=a
  "source /usr/share/vim-scripts/plugin/taglist.vim
  source ~/.vim/taglist.vim
  nnoremap <Leader>t :TlistToggle<CR>
  let Tlist_WinWidth = 35
  let Tlist_Sort_Type = 'name'
  let Tlist_Sort_Type = 'order'
  let Tlist_Display_Prototype = 1
  " Switch to tag item without delay.
  "autocmd BufEnter,CursorMoved,CursorMovedI * silent! TlistHighlightTag
  "autocmd BufEnter,CursorHold,CursorHoldI * silent! TlistHighlightTag
  set updatetime=1000

  let Tlist_Show_One_File = 1
"endif
