#!/bin/sh

set -x
set -e

find * -type d \( -name test \) -prune -o \
     -type f \( -name '*.[ch]' -o -name '*.cc' -o -name '*.cpp' -o -name '*.hpp' -o -name '*.s' -o -name '*.py' \) -print \
     > cscope.files

ctags -L cscope.files
cscope -b
gtags -f cscope.files

