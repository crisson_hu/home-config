" All system-wide defaults are set in $VIMRUNTIME/debian.vim (usually just
" /usr/share/vim/vimcurrent/debian.vim) and sourced by the call to :runtime
" you can find below.  If you wish to change any of those settings, you should
" do it in this file (/etc/vim/vimrc), since debian.vim will be overwritten
" everytime an upgrade of the vim packages is performed.  It is recommended to
" make changes after sourcing debian.vim since it alters the value of the
" 'compatible' option.

" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages available in Debian.
runtime! debian.vim

" Uncomment the next line to make Vim more Vi-compatible
" NOTE: debian.vim sets 'nocompatible'.  Setting 'compatible' changes numerous
" options, so any other options should be set AFTER setting 'compatible'.
"set compatible

" Vim5 and later versions support syntax highlighting. Uncommenting the
" following enables syntax highlighting by default.
if has("syntax")
  syntax on
endif
set viminfo=""

colors lightterm

" Uncomment the following to have Vim jump to the last position when
" reopening a file
"if has("autocmd")
"  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
"endif

" Uncomment the following to have Vim load indentation rules and plugins
" according to the detected filetype.
if has("autocmd")
  filetype plugin indent on
endif

" The following are commented out as they cause vim to behave a lot
" differently from regular Vi. They are highly recommended though.
set showcmd		" Show (partial) command in status line.
set showmatch		" Show matching brackets.
set ignorecase		" Do case insensitive matching
set smartcase		" Do smart case matching
set incsearch		" Incremental search
set hlsearch
set nu
set ic
set nows
set nowrap
"set autowrite		" Automatically save before commands like :next and :make
"set hidden             " Hide buffers when they are abandoned
"set mouse=a		" Enable mouse usage (all modes)
set ttym=xterm2
set modeline
" Make sure manually saving changes become habbit.
set nobackup
set noswapfile
set smartindent

"set nomodifiable

" Source a global configuration file if available
if filereadable("/etc/vim/vimrc.local")
  source /etc/vim/vimrc.local
endif

source ~/.vim/cscope_maps.vim
source ~/.vim/load-taglist.vim

"set paste
" first, enable status line always
set laststatus=2

" now set it up to change the status line based on mode
" FIXME it does NOT work
"if version >= 700
if has("syntax")
  "au InsertEnter * hi StatusLine term=reverse ctermbg=5 gui=undercurl guisp=Magenta
  "au InsertLeave * hi StatusLine term=reverse ctermfg=0 ctermbg=2 gui=bold,reverse
  "hi StatusLine term=reverse ctermbg=5 gui=undercurl guisp=Magenta
  "hi StatusLine term=reverse ctermfg=cyan ctermbg=0 gui=bold,reverse

  function! InsertStatuslineColor(mode)
    if a:mode == 'r'
      hi statusline guibg=magenta ctermfg=magenta
    elseif a:mode == 'i'
      hi statusline guibg=blue ctermfg=blue
    else
      hi statusline guibg=red ctermfg=red
    endif
  endfunction

  au InsertEnter * call InsertStatuslineColor(v:insertmode)
  "au InsertLeave * hi statusline guibg=yellow ctermbg=black ctermfg=cyan
  au InsertLeave * hi statusline guibg=yellow ctermfg=cyan

  " default the statusline to green when entering Vim
  hi statusline guibg=green ctermfg=cyan
endif
set ruler

"autocmd BufEnter *.cc,*.c,*.h,*.java,*.xs set fo=croq syntax smartindent cindent comments=sr:/*,mb:*,el:*/ textwidth=80
"set cinoptions=>8,e0,n0,f0,{0,}0,^0,:4,=4,p2,t2,+4,(4,)20,*30

set showtabline=2
nnoremap ,t :tabe<CR>

"nnoremap <Leader>c :set cursorline! cursorcolumn!<CR>
"nnoremap <Leader>c :set cursorline!<CR>
"set cursorcolumn
set cursorline

"augroup CursorLine
"  au!
"  au VimEnter,WinEnter,BufWinEnter * setlocal cursorline " nocursorcolumn
"  au WinLeave * setlocal nocursorline " nocursorcolumn
"augroup END

"set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab
"set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab
"set autoindent
