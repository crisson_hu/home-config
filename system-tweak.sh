#!/bin/sh

set -x

function systemctl_mask_now () {
    for i in apt-daily.timer \
                 apt-daily-upgrade.timer \
                 unattended-upgrades.service \
                 packagekit.service; do
        sudo systemctl mask --now ${i}
    done
}

function rm_f () {
    sudo rm -f /usr/bin/gnome-software
}

function run () {
    systemctl_mask_now
    rm_f
}

run
