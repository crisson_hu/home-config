#!/bin/bash


printf "CAUTION:\n"
printf "\tThis script will overwrite your config files in $HOME and system.\n"
printf "\n"
printf "Make sure you real want to do this.\n"
printf 'Installation will be started in 5 seconds, press Ctrl-C to stop ....\n'
sleep 6


set -x


######################################################################
# config files
CONFIG_FILES='
    aspell.conf
    bash_aliases
    bash_aliases_misc
    bash_logout
    bash_profile
    bashrc
    emacs.d
    gitignore
    screenrc
    tmux.conf
    toprc
    vim
    vimrc
    Xdefaults
'
function install_config {
    for f in ${CONFIG_FILES}; do
        INS=${HOME}/.${f}
        [ -f ${INS} ] || rm -fR ${INS}
        cp -r ${f} ${INS}
    done
}

install_config

# ~/bin
HOME_BIN_DIR=${HOME}/bin
[ -d ${HOME_BIN_DIR} ] || mkdir ${HOME_BIN_DIR}
for f in bin/*; do
    cp ${f} ${HOME_BIN_DIR}/../${f}
done

######################################################################
# install software
INS_PKG='vim emacs emacs-goodies-el elpa-magit global cscope xcscope-el exuberant-ctags'
function install_pkg {
    sudo apt -y install ${INS_PKG}
}

install_pkg

# config git ignore
git config --global core.excludesFile ~/.gitignore

# emacs
emacs -q --batch --eval '(require (quote package))' \
    --eval '(add-to-list (quote package-archives) (quote ("melpa" . "https://melpa.org/packages/")) t)' \
    --eval '(add-to-list (quote package-archives) (quote ("melpa-stable" . "https://stable.melpa.org/packages/")) t)' \
    --eval '(package-refresh-contents)' \
    --eval '(package-initialize)' \
    --eval '(package-install (quote ggtags))' \
    --eval '(package-install (quote rust-mode))' \
    --eval '(package-install (quote lsp-mode))' \
    --eval '(package-install (quote rustic))' \
    --eval '(package-install (quote elpy))' \
    --eval '(package-install (quote jedi))' \
    --eval '(package-install (quote rust-auto-use))'

ls -Fal ~/.emacs.d/elpa/

# some pkgs
sudo apt -y install command-not-found bmon
