(add-to-list 'load-path "~/.emacs.d/elpa/ggtags-0.8.10/")

(require 'ggtags)

(add-hook 'c-mode-common-hook
          (lambda ()
            (when (derived-mode-p 'c-mode 'c++-mode 'java-mode 'asm-mode)
              (ggtags-mode t)
	      (which-function-mode t))))

;; TODO
;; (add-hook 'dired-after-readin-hook
;; 	  (lambda ()
;; 	    ()))

(define-key ggtags-mode-map (kbd "C-c g s") 'ggtags-find-other-symbol)
(define-key ggtags-mode-map (kbd "C-c g h") 'ggtags-view-tag-history)
(define-key ggtags-mode-map (kbd "C-c g r") 'ggtags-find-reference)
(define-key ggtags-mode-map (kbd "C-c g f") 'ggtags-find-file)
(define-key ggtags-mode-map (kbd "C-c g c") 'ggtags-create-tags)
(define-key ggtags-mode-map (kbd "C-c g u") 'ggtags-update-tags)
(define-key ggtags-mode-map (kbd "C-c g d") 'ggtags-find-definition)

(define-key ggtags-mode-map (kbd "M-,") 'pop-tag-mark)
