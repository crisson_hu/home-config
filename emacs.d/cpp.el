;; cpp mode
(c-add-style "cpp"
             '((c-basic-offset . 4)
               (tab-width . 4)
               (c-tab-always-indent . t)
               ;; (indent-tabs-mode . nil)
               (ispell-check-comments . exclusive)
               (c-label-minimum-indentation . 0)
               (c-hanging-comment-starter-p . nil)
               (c-hanging-comment-ender-p . nil)))

;; (defun cpp-highlight-if-0/1 ()
;;   "Modify the face of text in between #if 0 ... #endif."
;;   (interactive)
;;   (setq cpp-known-face '(background-color . "dim gray"))
;;   (setq cpp-unknown-face 'default)
;;   (setq cpp-face-type 'dark)
;;   (setq cpp-known-writable 't)
;;   (setq cpp-unknown-writable 't)
;;   (setq cpp-edit-list
;;         '((#("1" 0 1
;;              (fontified nil))
;;            nil
;;            (background-color . "dim gray")
;;            both nil)
;;           (#("0" 0 1
;;              (fontified nil))
;;            (background-color . "dim gray")
;;            nil
;;            both nil)))
;;   (cpp-highlight-buffer t))

(defun cpp-highlight-if-0/1 ()
  "Modify the face of text in between #if 0 ... #endif."
  (interactive)
  (setq cpp-known-face 'default)
  (setq cpp-unknown-face 'default)
  (setq cpp-face-type 'dark)
  (setq cpp-known-writable 't)
  (setq cpp-unknown-writable 't)
  (setq cpp-edit-list
        '(;; (#("1" 0 1
          ;;    (fontified nil))
          ;;  nil
          ;;  (foreground-color . "gray60")
          ;;  both nil)
          (#("0" 0 1 (fontified nil))
           (foreground-color . "gray50")
           nil both nil)))
  (cpp-highlight-buffer t))

(defun jpk/c-mode-hook ()
  (cpp-highlight-if-0/1)
  (add-hook 'after-save-hook 'cpp-highlight-if-0/1 'append 'local))

(add-hook 'c-mode-common-hook 'jpk/c-mode-hook)

