(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; Comment/uncomment this line to enable MELPA Stable if desired. See `package-archive-priorities`
;; and `package-pinned-packages`. Most users will not need or want to do this.
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)

(global-font-lock-mode t)
;; (global-hl-line-mode t)
(transient-mark-mode t)
(show-paren-mode t)
(tool-bar-mode -1)
(menu-bar-mode -1)
;; (scroll-bar-mode -1)
;; (load-file "~/.emacs.d/tabbar.el")
;; (tabbar-mode t)
(ansi-color-for-comint-mode-on)
(setq visible-bell -1)
(auto-compression-mode t)
(auto-image-file-mode t)
(setq display-time-day-and-date t)
(setq display-time-24hr-format t)
(display-time)
(column-number-mode t)
(setq make-backup-files ())

(setq diff-switches "-u")
;; (setq ispell-program-name)

(iswitchb-mode t)
(blink-cursor-mode t)
;; (bar-cursor-mode t)

;; globally disable spaces turning into tabs
(setq-default indent-tabs-mode nil)

;; text mode
(add-hook 'text-mode-hook
          (lambda ()
            (setq indent-tabs-mode nil)))
;; picture mode
(add-hook 'picture-mode-hook
          (lambda ()
            (setq indent-tabs-mode nil)))
;; shell mode
(add-hook 'sh-mode-hook
          (lambda ()
            (setq indent-tabs-mode nil)))
;; html mode
(add-hook 'html-mode-hook
          (lambda ()
            (setq indent-tabs-mode nil)))
;; typescript mode
(add-hook 'javascript-mode-hook
          (lambda ()
            (setq indent-tabs-mode nil)))

;; java mode
(add-hook 'java-mode-hook
          (lambda ()
            "Treat Java 1.5 @-style annotations as comments."
            (setq c-comment-start-regexp "(@|/(/|[*][*]?))")
            (modify-syntax-entry ?@ "< b" java-mode-syntax-table)))
(add-hook 'java-mode-hook
          (lambda ()
            (setq c-basic-offset 4
                  tab-width 4
                  indent-tabs-mode nil
                  fill-column 90
                  comment-start "/* "
                  comment-end " */")))
(add-hook 'java-mode-hook (function cscope:hook))

;; common file hooks
(add-hook 'before-save-hook 'delete-trailing-whitespace)
;; (remove-hook 'post-command-hook 'global-hl-line-highlight)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(backup-inhibited t t)
 '(c++-mode-hook
   '((lambda nil
       (setq c-basic-offset 4)
       (setq tab-width 4)
       (setq indent-tabs-mode nil)
       (setq fill-column 90)
       (setq c-tab-always-indent t)
       (setq comment-start "/* ")
       (setq comment-end " */"))))
 '(compilation-read-command nil)
 '(compilation-scroll-output nil)
 '(compilation-window-height nil)
 '(default-frame-alist
    '((font . "Monospace-10")
      (cursor-color . "blue")
      (mouse-color . "white")
      (fullscreen . t)))
 '(dict-servers '("localhost"))
 '(dired-after-readin-hook '(ggtags-mode t))
 '(dired-garbage-files-regexp
   "\\(?:\\.\\(?:aux\\|bak\\|dvi\\|log\\|orig\\|rej\\|toc\\|pyc\\)\\)\\'")
 '(ggtags-global-abbreviate-filename nil)
 '(global-display-line-numbers-mode t)
 '(global-hi-lock-mode t)
 '(indent-tabs-mode t)
 '(inhibit-startup-echo-area-message t)
 '(inhibit-startup-screen t)
 '(initial-frame-alist '((fullscreen . maximized)))
 '(initial-scratch-message nil)
 '(ispell-extra-args nil)
 '(js-indent-level 2)
 '(js2-cleanup-whitespace t)
 '(kill-buffer-query-functions nil t)
 '(load-home-init-file t)
 '(magit-log-arguments
   '("--graph" "--color" "--decorate" "--show-signature" "-n256"))
 '(magit-push-arguments '("-v"))
 '(magit-rebase-arguments '("--interactive"))
 '(package-selected-packages
   '(jedi elpy rustic rust-auto-use rust-playground cargo xcscope tabbar session rust-mode pod-mode muttrc-mode mutt-alias markdown-mode magit initsplit htmlize graphviz-dot-mode ggtags folding eproject diminish csv-mode company color-theme-modern browse-kill-ring boxquote bm bar-cursor apache-mode))
 '(parens-require-spaces nil)
 '(python-shell-interpreter "python3")
 '(safe-local-variable-values
   '((encoding . utf-8)
     (py-indent-offset . 4)
     (c-indent-level . 8)
     (c-noise-macro-names "UNINIT")))
 '(sql-sqlite-program "sqlite3")
 '(stack-trace-on-error t)
 '(tags-case-fold-search nil)
 '(text-mode-hook '(text-mode-hook-identify))
 '(truncate-lines nil)
 '(vc-handled-backends nil)
 '(view-read-only t))

;; (if (string= system-type "windows-nt")
;;     (custom-set-variables
;;      '(default-frame-alist
;; 	'((font . "Lucida Console-11.5")
;; 	  (cursor-color . "blue")
;; 	  (mouse-color . "white")
;; 	  (fullscreen . t)))
;;      )
;;   (custom-set-variables
;;    '(default-frame-alist
;;       '(
;; 	(font . "Monospace-10")
;; 	(cursor-color . "blue")
;; 	(mouse-color . "white")
;; 	(fullscreen . t)))))

(put 'dired-find-alternate-file 'disabled nil)

(put 'narrow-to-region 'disabled nil)

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(font-lock-comment-face ((t (:foreground "DodgerBlue4" :weight normal))))
 '(font-lock-type-face ((t (:foreground "ForestGreen" :weight bold))))
 '(font-lock-warning-face ((t (:inherit secondary-selection :box (:line-width 2 :color "grey75" :style released-button) :weight bold))))
 '(highlight ((t (:underline (:color foreground-color :style wave)))))
 '(hl-line ((t (:inherit highlight :weight bold))))
 '(magit-diff-context-highlight ((t (:extend t :background "grey90" :foreground "grey20"))))
 '(magit-diff-hunk-heading ((t (:background "grey50" :foreground "grey90"))))
 '(magit-diff-hunk-heading-highlight ((t (:extend t :background "grey50" :foreground "grey95"))))
 '(magit-section-highlight ((t (:extend t :background "grey95"))))
 '(mode-line ((t (:box (:line-width 1 :color "SlateBlue1") :background "khaki"))))
 '(mode-line-inactive ((t (:box (:line-width 1 :color "SlateBlue1") :background "gray90"))))
 '(region ((t (:background "light goldenrod" :distant-foreground "gtk_selection_fg_color")))))

;; see http://www.gnu.org/software/emacs/manual/html_node/emacs/Standard-Faces.html
;; (set-face-background 'mode-line "light goldenrod")
;; (set-face-background 'mode-line "khaki")
;; (set-face-background 'mode-line-inactive "light cyan")
;; (set-face-background 'mode-line-inactive "gray90")

;; un/focus buffer
;; (add-hook 'focus-in-hook
;;        (lambda ()
;;          (set-background-color "gray96")))
;; (add-hook 'focus-out-hook
;;        (lambda ()
;;          (set-background-color "gray50")))

;; transparent
;; (defun toggle-transparency (&optional f)
;;   (interactive)
;;   (if (not (frame-parameter (selected-frame) 'alpha))
;;       (set-frame-parameter (selected-frame) 'alpha '(100 100)))
;;   (if (/=
;;        (cadr (frame-parameter (selected-frame) 'alpha))
;;        100)
;;       (set-frame-parameter (selected-frame) 'alpha '(100 100))
;;     (set-frame-parameter (selected-frame) 'alpha '(85 50))))

;; ggtags
;; (load-file "~/.emacs.d/load-ggtags.el")
(load-file "~/.emacs.d/cpp.el")
(load-file "~/.emacs.d/Sun-C-Style/wes.el")

;; markdown mode
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))

;; Cargo.toml
(add-to-list 'auto-mode-alist '("\\.toml\\'" . conf-mode))

;; vue mode
(add-to-list 'auto-mode-alist '("\\.vue\\'" . html-mode))
(add-to-list 'auto-mode-alist '("\\.template\\'" . html-mode))

;; typescript
(add-to-list 'auto-mode-alist '("\\.ts\\'" . javascript-mode))

(which-function-mode t)
(defun which-func-update ()
  ;; "Update the Which-Function mode display for all windows."
  (walk-windows 'which-func-update-1 nil 'visible))

;; move between windows
(global-set-key (kbd "<M-left>") 'windmove-left)
(global-set-key (kbd "<M-right>") 'windmove-right)
(global-set-key (kbd "<M-up>") 'windmove-up)
(global-set-key (kbd "<M-down>") 'windmove-down)
;; buffer move
(load-file "~/.emacs.d/buffer-move.el")
(global-set-key (kbd "<C-S-up>")     'buf-move-up)
(global-set-key (kbd "<C-S-down>")   'buf-move-down)
(global-set-key (kbd "<C-S-left>")   'buf-move-left)
(global-set-key (kbd "<C-S-right>")  'buf-move-right)

;; jedi
;; (add-to-list 'load-path "~/.emacs.d/emacs-jedi-master/")
;; (autoload 'jedi:setup "jedi" nil t)
;; (setq jedi:server-command '("~/.emacs.d/emacs-jedi-master/jediepcserver.py"))
;; (add-hook 'python-mode-hook 'jedi:setup)
;; (setq jedi:complete-on-dot t)

;; (yas-global-mode t)

;; rust-mode
(load-file "~/.emacs.d/load-rust-mode.el")

;; elpy
(load-file "~/.emacs.d/load-elpy.el")
