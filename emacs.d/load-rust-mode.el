(require 'rust-mode)

;; (setq rust-mode-font-lock-keywords
;;       (append
;;        `(
;; 	 ("\\_<async\\_>" . font-lock-keyword-face)
;; 	 )
;;        rust-mode-font-lock-keywords
;;        ))

(add-hook 'rust-mode-hook
          (lambda ()
            (setq indent-tabs-mode nil)))
