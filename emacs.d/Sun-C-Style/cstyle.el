;;
;;  cstyle.el
;;
;;  Author:  Matt Simmons <simmonmt@acm.org>
;;                        <simmonmt@eng.sun.com>
;;
;;  Canonical location:
;;
;;     http://installzone.eng/~simmonmt/cstyle/cstyle.el
;;
;;  Version: cstyle.el,v 1.8 1999/01/05 18:50:02 simmonmt Exp
;;
;;  This file contains functions that help with the cstyling of
;;  C code.
;;
;;  USAGE
;;
;;    M-x check-cstyle
;;      Checks the cstyle compliance of your code.  Lines out of compliance
;;      will be highlighted, and will be enumerated in a separate buffer.
;;      Click on the lines in that buffer with button2 to jump to the
;;      listed fault.
;;
;;      Navigation functions `cstyle-next-complaint' and
;;      `cstyle-previous-complaint' can be used in the buffer that was
;;      checked.  You should probably bind these to something.
;;
;;      `cstyle-explain-complaint' will explain the complaint for the
;;      non-compliant line under point.  Again, bind this to something.
;;
;;    M-x cstyle-space-after-cast
;;      Will cycle through the buffer, prompting you to change lines
;;      that will be flagged by cstyle as having spaces after the casts.
;;      This version uses a different regexp than cstyle itself does, and
;;      will catch a superset of the errors caught by cstyle.
;;
;;  INSTALLATION
;;
;;  It should work out of the box - just put it in your load-path and
;;  (require 'cstyle).  There are some variables you might want to
;;  modify:
;;
;;    cstyle-replace and cstyle-error - the faces used for hightlighting
;;    cstyle-command - the absolute path to the cstyle program
;;    cstyle-switches - the options to pass to cstyle
;;
;;  COMPATIBILITY
;;
;;  These functions have been developed on various betas of XEmacs 21.0.
;;  No testing is done on other Emacsen (or XEmacsen), but I don't use
;;  anything exotic, so it should work on other versions.  If something
;;  doesn't work, let me know.
;;
;;  TODO (in no particular order)
;;
;;    1. more correctors
;;    2. intelligent invocation of cstyle (different flags for header files,
;;       etc)
;;    3. Anything I haven't thought of yet.
;;

;;
;; Things you might want to override in your ~/.emacs
;;

;; Define the faces.  On Emacsen with defface defined, the defface forms
;; will be executed.  This will allow you to customize the faces with
;; `customize-face'.  Emacsen without defface will get the faces created
;; without customize.
(eval-and-compile
  (cond ((fboundp 'defface)
	 ;; Emacsen with defface
	 (defface cstyle-replace '((((class color))
				    (:background "paleturquoise"))
				   (t
				    (:inverse-video t)))
	   "Face for highlighting replacements cstyle wants to make.")
	 
	 (defface cstyle-error '((((class color) (background dark))
				  (:background "darkred"))
				 (((class color) (background light))
				  (:background "LightPink2"))
				 (t
				  (:underline t)))
	   "Face for highlighting entire lines that contain cstyle errors."))
	(t
	 ;; Emacsen without defface
	 (and (not (find-face 'ctsyle-replace))
	      (make-face 'cstyle-replace
		"Face for highlighting replacements cstyle wants to make.")
	      (set-face-background 'cstyle-replace "paleturquoise"))

	 (and (not (find-face 'cstyle-error))
	      (make-face 'cstyle-error
		"Face for highlighting entire lines that contain cstyle errors.")
	      (set-face-background 'cstyle-error "red")))))

;; Miscellaneous control variables
(defvar cstyle-command "/opt/gate/public/bin/cstyle"
  "The program to run to check code for cstyle compliance.")
(defvar cstyle-switches "-p"
  "Flags to pass to the function in `cstyle-command'.")
(defvar cstyle-buffer-window-height 5
  "The maximum height (in lines) of the cstyle output buffer.")

;;
;; Things you shouldn't need to change
;;

(defvar cstyle-buffer-name "*cstyle-out*")

(defun check-cstyle (&optional buffer)
  "*Check a buffer for cstyle-compliance."
  (interactive)

  (let ((cstyle-buffer (get-buffer-create cstyle-buffer-name))
	(original-buffer (or buffer (current-buffer)))
	code-highlight output-highlight
	line-num line-complaint)

    ;; Clean up after previous run
    (cstyle-delete-complaints original-buffer)
    (erase-buffer cstyle-buffer)

    (message "Checking buffer...")
    (shell-command-on-region (point-min) (point-max)
			     (concat cstyle-command
				     (if cstyle-switches
					 (concat " " cstyle-switches)
					 nil)) cstyle-buffer)
    (if (not (buffer-live-p cstyle-buffer))
	(message "No changes required")

      (message "Marking changes...")

      (save-excursion
	(goto-char (point-min) original-buffer)
	(set-buffer cstyle-buffer)
	(goto-char (point-min))
	(while (search-forward-regexp "^<stdin>: \\([0-9]+\\): \\(.*\\)$" nil t)
	  (setq line-num (string-to-int (match-string 1))
		line-complaint (match-string 2))

	  ;; Create the extent used for highlighting the code
	  (setq code-highlight (make-extent nil nil))
	  (set-extent-property code-highlight 'face 'cstyle-error)
	  (set-extent-property code-highlight 'cstyle-type 'error)
	  (set-extent-property code-highlight 'cstyle-complaint line-complaint)
	  (set-extent-endpoints code-highlight
				(cstyle-point-at-bol line-num original-buffer)
				(cstyle-point-at-eol line-num original-buffer)
				original-buffer)

	  ;; Create the extent used for highlighting the cstyle output
	  (setq output-highlight (make-extent nil nil))
	  (set-extent-property output-highlight 'mouse-face 'highlight)
	  (set-extent-property output-highlight
			       'keymap (cstyle-make-extent-map
					code-highlight original-buffer))
	  (set-extent-endpoints output-highlight
				(match-beginning 0) (match-end 0) cstyle-buffer)))

      ;; Shrink the window to
      ;; (min contents-height cstyle-buffer-window-height)
      (let ((cstyle-window (get-buffer-window cstyle-buffer)))
	(if (and cstyle-window
		 (not (buffer-in-multiple-windows-p cstyle-buffer)))
	    (shrink-window (- (window-displayed-height cstyle-window)
			       cstyle-buffer-window-height)
			    nil cstyle-window))
	(shrink-window-if-larger-than-buffer cstyle-window))
	   
      (message "Marking changes...done"))))

(defsubst cstyle-make-extent-map (extent buffer)
  (let ((map (make-sparse-keymap)))
    (define-key map [(button2)] `(lambda ()
				   (interactive)
				   (cstyle-goto-extent ,extent ,buffer)))
    map))

(defun cstyle-goto-extent (extent buffer)
  (cond ((and (extent-live-p extent)
	      (buffer-live-p buffer))
	 (set-buffer buffer)
	 (goto-char (extent-start-position extent) buffer))
	(t
	 (error "Code highlight has vanished!"))))

(defun cstyle-next-complaint (&optional pos type)
  "Move point to the beginning of the next cstyle extent in the current buffer."
  (interactive "d")

  (let (next-complaint top-complaint)

    ;; Look for the next cstyle extent
    (map-extents (lambda (x arg)
		   (let (extent-start)
		     (setq extent-start (extent-start-position x))

		     ;; Find the one closest to pos but after it
		     (if (and (< pos extent-start)
			      (or (not next-complaint)
				  (> (extent-start-position next-complaint)
				     extent-start)))
			 (setq next-complaint x))

		     ;; Find the topmost one
		     (if (or (not top-complaint)
			     (> (extent-start-position top-complaint)
				extent-start))
			 (setq top-complaint x)))
		   nil)
		 (current-buffer) (point-min) (point-max) nil nil
		 'cstyle-type type)

    ;; If there's one after pos, go to it (and display the complaint).  If
    ;; not, go to the first one in the buffer.
    (cond
     (next-complaint
      (goto-char (extent-start-position next-complaint)))
     (top-complaint
      (goto-char (extent-start-position top-complaint)))
     (t
      (error "No other complaint")))
    (cstyle-explain-complaint)))

(defun cstyle-previous-complaint (&optional pos type)
  "Move point to the end of the previous cstyle extent in the current buffer."
  (interactive "d")

  (let (prev-complaint bottom-complaint)

    ;; Look for the previous cstyle extent
    (map-extents (lambda (x arg)
		   (let (extent-end)
		     (setq extent-end (extent-end-position x))

		     ;; Find the one closest to pos but before it
		     (if (and (> pos extent-end)
			      (or (not prev-complaint)
				  (< (extent-end-position prev-complaint)
				     extent-end)))
			 (setq prev-complaint x))

		     ;; Find the bottommost one
		     (if (or (not bottom-complaint)
			     (< (extent-end-position bottom-complaint)
				extent-end))
			 (setq bottom-complaint x)))
		   nil)
		 (current-buffer) (point-min) (point-max) nil nil
		 'cstyle-type type)

    ;; If there's one before pos, go to it (and display the complaint.  If
    ;; not, go to the bottom one in the buffer.
    (cond
     (prev-complaint
      (goto-char (extent-start-position prev-complaint)))
     (bottom-complaint
      (goto-char (extent-start-position bottom-complaint)))
     (t
      (error "No other complaint")))
    (cstyle-explain-complaint)))

(defun cstyle-explain-complaint (&optional pos)
  "Display the reason for the cstyle complaint under point."

  (let ((x (extent-at (or pos (point)) nil 'cstyle-type nil 'at)))
    (if x
	(message (concat "cstyle complaint: "
			 (extent-property x 'cstyle-complaint)))
      (error "No cstyle complaint under point"))))

(defun cstyle-delete-complaints (&optional buffer type)
  "*Delete cstyle complaints in a buffer.
All extents with the `cstyle-type' property set to the value of
`type', or any extent with the `cstyle-type' property set non-nil if
`type' is nil, will be deleted."
  (interactive)
  (if (not buffer) (setq buffer (current-buffer)))
  
  (map-extents (lambda (x type)
		 (if type
		     (if (eq (extent-property x 'cstyle-type) type)
			 (delete-extent x))
		   (if (extent-property x 'cstyle-type)
		       (delete-extent x))))
	       buffer (point-min buffer) (point-max buffer) type))

(defun cstyle-space-after-cast (goto-beginning)
  "*Fix the cstyle `space after cast' errors.
If given a prefix argument (or a non-nil `goto-beginning'), it
will fix from the beginning of the buffer."
  (interactive "P")

  ;; If they gave a prefix, start from the beginning
  (if goto-beginning
      (goto-char (point-min)))

  ;; Make the face, if it hasn't already been made
  (copy-face 'isearch 'cstyle-replace)

  (let ((highlight (make-extent nil nil))
	newstr)
    (set-extent-property highlight 'face 'cstyle-replace)
    (set-extent-priority highlight (1+ mouse-highlight-priority))
    
    (unwind-protect
	(while (search-forward-regexp
		"\\(([A-Za-z0-9_]+\\( \\*+\\)?)\\) \\([A-Za-z_]\\|&[^ &]\\)" nil t)
	  (unless (string= (match-string 1) "(void)")
	    (set-extent-endpoints highlight
				  (match-beginning 0)
				  (match-end 0))
	    
	    (setq newstr (concat (match-string 1) (match-string 3)))
	    (when (yes-or-no-p (format "Replace `%s' with `%s'? "
				       (concat (match-string 1) " "
					       (match-string 3))
				       newstr))
	      (detach-extent highlight)
	      (replace-match newstr)
	      (undo-boundary))))
      
      (if (extent-live-p highlight)
	  (delete-extent highlight)))))

;;
;; Hacks to make it work with older versions of Emacs
;;
(eval-and-compile
  (fset 'cstyle-point-at-bol
	(if (fboundp 'point-at-bol) 'point-at-bol
	  'cstyle-point-at-bol-impl))

  (fset 'cstyle-point-at-eol
	(if (fboundp 'point-at-eol) 'point-at-eol
	  'cstyle-point-at-eol-impl)))

(defun cstyle-point-at-bol-impl (&optional arg buffer)
  "An implementation of `point-at-bol' for Emacsen that don't have it."
  (save-excursion
    (and buffer
	 (set-buffer buffer)
	 (goto-char (point-min)))
      
    (beginning-of-line arg)
    (point)))

(defun cstyle-point-at-eol-impl (&optional arg buffer)
  "An implementation of `point-at-eol' for Emacsen that don't have it."
  (save-excursion
    (and buffer
	 (set-buffer buffer)
	 (goto-char (point-min)))
    
    (end-of-line arg)
    (point)))

(provide 'cstyle)
