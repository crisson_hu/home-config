;;
;; check for, and clean up, some cstyle annoyances i always slip up on..
;;

(defun cstyle-match (pattern)
  (save-excursion
    (goto-char (point-min))
    (re-search-forward pattern nil t)))

(defun cstyle-check ()
  (cond ((cstyle-match "^#define ")
	 "#define followed by space instead of tab")
	((cstyle-match "[ \t]+$")
	 "Whitespace at end of line")
	((cstyle-match "\\s-sizeof(")
	 "Missing space after sizeof")))

(defun cstyle-fix-define ()
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward "^#define " nil t)
      (replace-match "#define\t"))))

(defun cstyle-fix-sizeof ()
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward "\\s-sizeof(" nil t)
      (replace-match "sizeof ("))))

(defun cstyle-fix-whitespace ()
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward "[ \t]+$" nil t)
      (replace-match ""))))

(defun cstyle-fix ()
  (cstyle-fix-define)
  (cstyle-fix-sizeof)
  (cstyle-fix-whitespace))

(defun cstyle-me ()
  (interactive)
  (let ((complaint (cstyle-check)))
    (cond ((and complaint
		(y-or-n-p (format "%s. Do you want to correct the style? " complaint)))
	   (cstyle-fix)
	   (message "Style issue fixed.")))))

(provide 'cstyle-cleanup)
