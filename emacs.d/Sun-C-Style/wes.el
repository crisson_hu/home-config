(load "cc-mode")
(load "cc-fonts")
(load "~/.emacs.d/Sun-C-Style/cstyle.el")
(load "~/.emacs.d/Sun-C-Style/cstyle-cleanup.el")


;;;; lifted from Bill Sommerfeld's $HOME/.emacs

(defvar c-knf-style nil
  "C indentation style implementing Kernel Normal Form")

(defvar c-epi-style nil)

(setq c-knf-style '("knf" "bsd" (c-basic-offset . 8)
                    (c-hanging-braces-alist . ((topmost-intro before after)
                                               (substatement-open after)
                                               (brace-list-open)
                                               (block-close . c-snug-do-while)
                                               (extern-lang-open after)))
                    (c-hanging-comment-starter-p . nil)
                    (c-cleanup-list brace-else-brace brace-elseif-brace
                                    list-close-comma scope-operator
                                    defun-close-semi)
                    (c-comment-continuation . "* ")
                    (c-hanging-comment-ender-p . nil)
                    (c-offsets-alist
                     (arglist-cont . *)
                     (arglist-close . *)
                     (arglist-cont-nonempty . *)
                     (statement-cont . *))))

(load-library "cc-mode")

(defun wes:c-lineup-continuation (langelem)
  (save-excursion
    (let ((indent-point (cdr langelem)))
      (goto-char indent-point)
      (beginning-of-line nil)
      (skip-chars-forward " \t")
      (vector (+ (/ c-basic-offset 2) (current-column))))))

(c-add-style "sun" '(
                     (c-basic-offset . 8)
                     (c-tab-always-indent . t)
                     (ispell-check-comments . exclusive)
                     (c-label-minimum-indentation . 0)
                     (c-hanging-comment-starter-p . nil)
                     (c-hanging-comment-ender-p . nil)
                     (c-offsets-alist
                      (inextern-lang . -)
                      (arglist-cont . wes:c-lineup-continuation)
                      (arglist-cont-nonempty . wes:c-lineup-continuation)
                      (statement-cont . *)
                      (label . -1000))
                     ))

(c-add-style "sfx"
	     '((c-basic-offset . 4)
	       (tab-width . 4)
	       (c-tab-always-indent . t)
	       (indent-tabs-mode . nil)
	       (ispell-check-comments . exclusive)
	       (c-label-minimum-indentation . 0)
	       (c-hanging-comment-starter-p . nil)
	       (c-hanging-comment-ender-p . nil)
	       (c-offsets-alist
		(arglist-cont . 8)
		(arglist-cont-nonempty . 12)
		(arglist-close . 0)
		(label . -)
		(statement-cont . 4))))

(c-add-style "aerospike"
	     '((c-basic-offset . 8)
	       (tab-width . 8)
	       (c-tab-always-indent . t)
	       (indent-tabs-mode . t)
	       (ispell-check-comments . exclusive)
	       (c-label-minimum-indentation . 0)
	       (c-hanging-comment-starter-p . nil)
	       (c-hanging-comment-ender-p . nil)
	       (comment-start . "//")
	       (comment-end . "")
	       (c-offsets-alist
		(arglist-cont . 8)
		(arglist-cont-nonempty . 16)
		(arglist-close . 0)
		(label . -)
		(statement-cont . 16))))

(setq c-epi-style '("epi" "gnu"
                    (c-hanging-braces-alist . ((substatement-open before after)
                                               (brace-list-open)
                                               (block-close . c-snug-do-while)
                                               (extern-lang-open after)))))

;; implement C style policy based on which CVS repository we're looking at..

(defun wes-guess-style-from-cvs ()
  (let ((cvs-root-file (expand-file-name "CVS/Root" default-directory))
        (sccs-dir-name (expand-file-name "SCCS" default-directory)))
    (cond ((file-directory-p sccs-dir-name) "sun")
          ((file-readable-p cvs-root-file)
           (save-excursion
             (set-buffer (find-file-noselect cvs-root-file))
             (cond ((looking-at "^cvs.netbsd.org") "knf")
                   ((looking-at "^cvs.epilogue.com") "epi")
                   (t nil)))))))

(defun wes-guess-style-from-pathname ()
  (cond ((string-match "/epilogue/" default-directory) "epi")
	((string-match "/scaleflux/" default-directory) "sfx")
	((string-match "/aerospike/" default-directory) "aerospike")
        (t nil)))

(defun wes-guess-c-style ()
  (or (wes-guess-style-from-cvs)
      (wes-guess-style-from-pathname)
      "sun"
      "knf"))

(defun wes-update-c-style (style-entry)
  (let* ((tag (car style-entry))
         (cur (assoc tag c-style-alist)))
    (cond ((null cur)
           (setq c-style-alist (cons style-entry c-style-alist)))
          ((equal cur style-entry)
           nil)
          (t
           (rplacd cur (cdr style-entry))))))

(defun wes-update-c-styles ()
  (interactive)
  (wes-update-c-style c-knf-style)
  (wes-update-c-style c-epi-style))

(defun wes-c-mode-hook ()
  (define-key c-mode-map "\r" 'newline-and-indent)
  (wes-update-c-styles)
  (c-set-style (wes-guess-c-style))
  (setq c-tab-always-indent nil)
  (c-toggle-auto-hungry-state 1)
  (require 'cstyle-cleanup)
  (setq rm-trailing-spaces 1)
  (if (string= (wes-guess-c-style) "sun")
      (add-hook 'local-write-file-hooks 'cstyle-me)))

(add-hook 'c-mode-common-hook 'wes-c-mode-hook)

(defun wes-asm-mode-hook ()
  (require 'wes-asm))

(add-hook 'asm-mode-hook 'wes-asm-mode-hook)


;; font-face
(add-hook 'prog-mode-hook
	  (lambda ()
	    (font-lock-add-keywords nil
	     '(("\\<\\(FIXME\\|fixme\\|TODO\\|todo\\|BUG\\):?" 1 'font-lock-warning-face prepend)
	       ("\\<\\(and\\|or\\|not\\)\\>" . 'font-lock-keyword-face)))))

;;;; end of sommerfeld-lifting
