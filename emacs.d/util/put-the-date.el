(defun put-the-date ()
  (interactive)
  (insert (format-time-string "%a %Y-%m-%d" (time-to-seconds (current-time)))))

(global-set-key (kbd "C-c C-d") 'put-the-date)
